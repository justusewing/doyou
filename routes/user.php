<?php

Route::post('/users', 'UserController@handles')->name('handles');

Route::prefix('user')->group(function () {
	Route::get('/view/{user}', 'UserController@profile')->name('user');
	Route::get('/mentions/{user}', 'UserController@mentions')->name('user-mentions');

	Route::prefix('engage')->group(function () {
		Route::get('/follow/{user}', 'UserController@follow')->name('follow');
		Route::get('/unfollow/{user}', 'UserController@unfollow')->name('unfollow');
		Route::get('/connect/{user}', 'UserController@connect')->name('connect');
		Route::get('/disconnect/{user}', 'UserController@disconnect')->name('disconnect');
	});

	
});