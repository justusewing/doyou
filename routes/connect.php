<?php

Route::prefix('connections')->group(function () {
	Route::get('/all/{user}', 'ConnectionController@connections')->name('connections');
	Route::get('/accept/{user}', 'ConnectionController@acceptConnect')->name('acceptConnect');
});