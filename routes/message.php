<?php


Route::prefix('messages')->group(function () {
	Route::get('/', 'MessageController@messages')->name('user-messages');
	Route::get('/compose', 'MessageController@compose')->name('compose');
	Route::get('/conversation/with/{user}', 'MessageController@conversation')->name('conversation');
	Route::get('/conversation/fetch/{user}', 'MessageController@fetchConversation');
	Route::post('/conversation', 'MessageController@message')->name('message');

});