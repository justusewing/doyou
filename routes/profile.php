<?php

Route::prefix('profile')->group(function () {
	Route::get('/', function() { return view('layouts.profile.user'); })->name('user-profile');
	Route::get('/edit/{id}', 'ProfileController@editProfileView')->name('view-edit-profile');
	Route::post('/edit', 'ProfileController@editProfile')->name('editProfile');
	Route::post('finish', 'ProfileController@finishProfile')->name('finishProfile');
});

// check if username exists

Route::post('/confirm/username/', 'ProfileController@confirmUsername')->name('confirmUsername');