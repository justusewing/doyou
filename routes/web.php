<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@init')->name('home');

Route::get('/trend/{tag}', 'TrendController@hashtag')->name('hashtag');



Auth::routes();

// all requests connected to /achievements
include('achievements.php');

// all requests connected to /attributes
include('attributes.php');

// all requests connected to /community
include('community.php');

// all requests connected to /connector
include('connect.php');

// all requests connected to /experiences
include('experiences.php');

// all requests connected to /market
include('market.php');

// all requests connected to /message
include('message.php');

// all requests connected to /network
include('network.php');

// all requests connected to /notification
include('notification.php');

// all requests connected to /podium
include('podium.php');

// all requests connected to /posts
include('posts.php');


// all requests connected to /profile
include('profile.php');

// all requests connected to /relationships
include('relationships.php');

// all requests connected to /shine
include('shine.php');

// all requests connected to /user
include('user.php');

/* 
|------------------
| Other Routes
|------------------
*/

Route::get('/search', 'HomeController@search')->name('search');

Route::get('/identity', function () {
    return view('useridentity');
});

Route::get('/references', function () {
    return view('userreferences');
});
Route::get('/history', function () {
    return view('userhistory');
});

Route::get('/favorites', function () {
    return view('userfavorites');
});
Route::get('/primer', function () {
    return view('userprimer');
});

// allow the user to logout by sending get request to this path
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/about', function () {
    return view('about');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
