<!-- Vertical form modal -->
<div id="modal_publish" class="modal fade" style="margin-top:30%;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Post to the world</h5>
			</div>
			<div class="modal-body">
				@include('layouts.publish')
			</div>
		</div>
	</div>
</div>
<!-- /vertical form modal -->

<!-- Vertical form modal -->
<div id="modal_search" class="modal fade" style="margin-top:30%;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Looking for something?</h5>
			</div>
			<div class="modal-body">
				
			</div>
		</div>
	</div>
</div>
<!-- /vertical form modal -->