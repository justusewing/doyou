@extends('layouts.application')

@section('content')
<!-- Single mail -->
							<div class="panel panel-white" style="width:400px; height:400px; margin: auto auto; margin-top:15%;">

								<!-- Mail toolbar -->
								<div class="panel-toolbar panel-toolbar-inbox" style="background: none;">
									<div class="navbar navbar-default">
									<i class="far fa-comments" style="width:200px; margin: 0 auto; display: block; text-align: center; font-size: 100px; margin-top:30%; color:#2196F3;"></i>
										<ul class="nav navbar-nav visible-xs-block no-border">
											<li>
												
											</li>
										</ul>
									</div>
								</div>
								<!-- /mail toolbar -->


								<!-- Mail details -->
								<div class="table-responsive mail-details-write" style="padding:20px;">
									<select data-placeholder="Select a User..." class="select-size-lg" id="users">
										<option></option>
										<optgroup label="Following">
											@foreach($followings as $following)

											<option value="{{ $following->following }}">{{ "@" . $following->identity($following->following)->profile->handle }}</option>
												}
											@endforeach
										</optgroup>
										<optgroup label="Followers">
											@foreach($followers as $follower)
											<option value="{{ $follower->user->id }}">{{ "@" . $follower->user->profile->handle }}</option>
											@endforeach
										</optgroup>
										<optgroup label="Connections">
											@foreach($connections as $connection)
											<option value="{{ $connection->user->id }}">{{ "@" . $connection->user->profile->handle }}</option>
											@endforeach
										</optgroup>
									</select>
								</div>
								<!-- /mail details -->
</div>
@endsection

@section('js')
	<script type="text/javascript" src="/assets/js/pages/form_select2.js"></script>
	<script type="text/javascript">
	$("#users").val("");
		$("#users").change(function(){
			window.location.href = '{{ route("conversation", ["user" => null])}}/' + $(this).val();
		});
	</script>
@endsection