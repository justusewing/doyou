@extends('layouts.application')

@section('content')
    <!-- Multiple lines -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">My Inbox</h6>
        </div>

        <div class="panel-toolbar panel-toolbar-inbox">
            <div class="navbar navbar-default">
                <ul class="nav navbar-nav visible-xs-block no-border">
                    <li>
                        <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-multiple">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </li>
                </ul>

                <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-multiple">
                <!--
                    <div class="btn-group navbar-btn">
                        <button type="button" class="btn btn-default btn-icon btn-checkbox-all">
                            <input type="checkbox" class="styled">
                        </button>

                        <button type="button" class="btn btn-default btn-icon dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu">
                            <li><a href="#">Select all</a></li>
                            <li><a href="#">Select read</a></li>
                            <li><a href="#">Select unread</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Clear selection</a></li>
                        </ul>
                    </div>
                -->

                    <div class="btn-group navbar-btn">
                        <a href="{{ route('compose') }}" class="btn btn-default"><i class="icon-pencil7"></i> <span class="hidden-xs position-right">Compose</span></a>
                        <!--
                        <button type="button" class="btn btn-default"><i class="icon-bin"></i> <span class="hidden-xs position-right">Delete</span></button>
                        <button type="button" class="btn btn-default"><i class="icon-spam"></i> <span class="hidden-xs position-right">Spam</span></button>
                        -->
                    </div>
                </div>
            </div>
        </div>


        <div class="table-responsive">
            <table class="table table-inbox">
                <tbody data-link="row" class="rowlink">
                    @foreach($messages as $message)
                        <tr class="unread">
                            <td class="table-inbox-checkbox rowlink-skip">
                                <input type="checkbox" class="styled">
                            </td>
                            <td class="table-inbox-star rowlink-skip">
                                <a href="{{ route('conversation', ['user' => $message->conversationWithId()]) }}">
                                    <i class="icon-star-empty3 text-muted"></i>
                                </a>
                            </td>
                            <td class="table-inbox-image">
                                <img src="{{ $message->conversationWithAvatar() }}" class="img-circle img-xs" alt="">
                            </td>
                            <td class="table-inbox-name">
                                <a href="#">
                                    <div class="letter-icon-title text-default">{{ $message->conversationWithName() }} </div>
                                </a>
                            </td>
                            <td class="table-inbox-message">
                                <span class="table-inbox-subject">{{ $message->message }}</span>
                            </td>
                            <td width="100">
                                {{ convertTimeStamp($message->created_at) }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
