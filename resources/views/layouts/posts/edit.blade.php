@extends('layouts.application')

@section('css')
    <style>
                #reset{
            display: none;
        }
    </style>
@endsection

@section('content')
    <form method="POST" action="{{ route('update-post') }}" enctype="multipart/form-data" id="new-post">   
        @csrf
        <input type="hidden" name="post_id" value="{{ $post->id }}">
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <div id="bannerPreview" class="preview">
                    @if(isset($post->featured))
                    <div class="thumb content-group" style="margin-bottom: 0 !important;">
                        <img src="/uploads/media/{{ $post->featured() }}" style="border-radius: 0; max-width: 300px;">
                    </div>
                    @endif
                    </div>
                    <br>
                    <div class="text-left">
                        <label class="btn bg-danger text-white" id="reset"><i class="fa fa-times"></i> Remove Image</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <textarea class="form-control mention" name="content" placeholder="Share what's on your mind..." id="post-text">{{ $post->content }}</textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <input type="checkbox" name="remove_featured" value="1"> Remove Featured Image
                    <input type="file" class="file-styled" name="media" id="banner">
                </div>
            </div>
        </div>
        <div class="form-group text-right">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn bg-indigo btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Update Post</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/parsers.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="/assets/js/pages/editor_wysihtml5.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/media/validate.js"></script>
    <script type="text/javascript">
        
    $("#url").keyup(function(){
        var val = $(this).val();
        var last = val.substr(val.length - 1);
        if(last == " "){
                $(this).val(val.slice(0,-1) + "-");
        }

        $("#urlPreview").text($(this).val());

        confirmPodURL($(this).val());
    });

    function confirmPodURL(url){
        $.post( "{{ route('confirmPodURL') }}", { url: url, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){
                if(data == "ok"){
                    $('#urlgroup').addClass("has-success");
                    $('#urlgroup').removeClass("has-error");
                    $('#submit').removeClass("disabled");
                } else{
                    alert("This URL is taken!");
                    $('#urlgroup').addClass("has-error");
                    $('#urlgroup').removeClass("has-success");
                    $('#submit').addClass("disabled");
                }
        });
    }



    </script>
@endsection
