<div class="sidebar sidebar-main sidebar-fixed">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <div class="circle up @if($page == 'home') active @endif" onclick="window.location.href">
                        <a href="/"><i class="fa fa-home"></i></a>
                    </div>
                    <div class="circle right @if($page == 'connect') active @endif">
                        <a href="{{ route('connections', ['id' => Auth::user()->id]) }}"><i class="fa fa-hands-helping"></i></a>
                    </div>
                    <div class="circle down">
                        <a href="/market"><i class="fa fa-store"></i></a>
                    </div>
                    <div class="circle left @if($page == 'shine') active @endif">
                        <a href="/shine"><i class="fa fa-star"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- Main -->
                    <li class="@if($page == 'home') active @endif"><a href="/"><i class="icon-home4"></i> <span>Main</span></a></li>
                    <li class="{{ $podium or '' }}"><a href="{{ route('user-podium') }}"><i class="icon-comment"></i> <span>Podium</span></a></li>
                    <li><a href="#"><i class="icon-users"></i> <span>Community</span></a></li>
                    <li><a href="#"><i class="fa fa-star"></i> <span>Favorites</span></a></li>
                    <li><a href="#"><i class="fa fa-chart-bar"></i> <span>Analytics</span></a></li>
                    <li class="ad-unit">
                        <a href="">
                            <img src="https://s3.envato.com/files/181151801/jpg/240x400_Vertical-Rectangle.jpg">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>