@extends('layouts.application')

@section('content')
<div class="row">
<div class="col-md-8">
	<div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Active Connections</h6>

            <div class="heading-elements not-collapsible">
            </div>
        </div>
        <div class="panel-body">
        		<ul class="media-list media-list-linked">
					@foreach($activeConnections as $activeConnection)
						<li class="media">
							<a href="{{ route('user', ['user' => $activeConnection['connected']['profile']['user_id']]) }}" class="media-link">
								<div class="media-left"><img src="{{ getAvatar($activeConnection['connected']['profile']['user_id']) }}" class="img-circle img-md" alt=""></div>
								<div class="media-body">
									<div class="media-heading text-semibold">{{ "@" . $activeConnection["connected"]["profile"]["handle"] }}</div>
									<span class="text-muted">{{ $activeConnection["connected"]["profile"]["location"] }}</span>
								</div>
								<div class="media-right media-middle">
									<span class="label label-primary"></span> &nbsp; &nbsp;
								</div>
							</a>
						</li>
					@endforeach
				</ul>
       </div>
      </div>
</div>
        	<div class="col-md-4">
        	<div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Pending Connections</h6>

            <div class="heading-elements not-collapsible">
            </div>
        </div>
        <div class="panel-body">
        		<ul class="media-list media-list-linked">
					@foreach($pendingConnections as $pendingConnection)
						<li class="media">
							<a href="#" class="media-link">
								<div class="media-left"><img src='{{ $pendingConnection->user->avatar() }}' class="img-circle img-md" alt=""></div>
								<div class="media-body">
									<div class="media-heading text-semibold">{{ "@" . $pendingConnection->user->profile->handle }}</div>
									<span class="text-muted">{{ $pendingConnection->user->profile->location }}</span>
								</div>
								<div class="media-right media-middle">
									<span onclick=" window.location.href='{{ route('acceptConnect', ['user' => $pendingConnection->user_id ]) }}'" class="label label-primary">Accept</span>
								</div>
							</a>
						</li>
					@endforeach
				</ul>
		</div>
	</div>
	</div>
</div>
@endsection

@section('js')

@endsection