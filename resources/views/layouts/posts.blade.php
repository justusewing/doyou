<!-- Blog layout #4 with image -->
    <div class="panel panel-flat col-md-12">
        <div class="panel-heading">
            <h6 class="panel-title" style="position:absolute; right: 20px; z-index: 4;">{{ convertTimeStamp($post->created_at) }} <button type="button" class="btn border-warning text-warning-600 btn-float btn-float-lg btn-rounded @if(Auth::user()->likesPost($post->id)) unlike @else like @endif" style="margin-left:20px;" data-post="{{ $post->id }}"><i class="far fa-heart"></i></button></h6>
            <h6 class="panel-title" style="position:absolute; left: 20px; z-index: 4;"><a href="{{ route('user', ['id' => $post->user]) }}"><img src="{{ $post->user->avatar() }}" class="post-avatar img-circle" alt=""> {{ "@" . $post->user->profile->handle }}</a> @if($post->isOwner())<span class=""><a href="{{ route('edit-post', ['post' => $post->id]) }}"><i class="fa fa-edit"></i> Edit</a></span> <span class="deletePost" data-post="{{ $post->id }}"><i class="fa fa-trash"></i></span> @endif</h6>
        </div>
        <div class="panel-body">
            <div class="row" style="@if(!isset($post->featured))background-image: url('/assets/images/text-post.jpg'); background-size: cover; background-position: left bottom;@endif">
                <div class="col-md-12 view-post" data-url="{{ route('view-post', ['post' => $post->id]) }}">
                    @if(isset($post->featured))
                    <div class="thumb content-group" style="margin-bottom: 0 !important;">
                        <h5 class="text-left" style="padding-left:20px;">{! linkHashes($post->content) !}</h5>
                        <img src="/uploads/media/{{ $post->featured() }}" style="border-radius: 0;">
                    </div>
                    @else
                    <div class="thumb content-group">
                        <h2 style="line-height: 200px;"><span class="quote"><i class="fa fa-quote-left"></i></span> {!! linkHashes($post->content) !!} <span class="quote"><i class="fa fa-quote-right"></i></span></h2>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <textarea type="text" class="form-control comment mention" style="border:none; border-radius: 0;" placeholder="Say something..." data-post="{{ $post->id }}" ></textarea>
            <div class="row">
                <div class="col-md-12">
                    <div class="comment-list">
                        <ul class="list-feed list-feed-solid list-feed-comments">
                            @foreach($post->comments as $comment)
                                <li class="border-primary-300">
                                    <a href="{{ route('user', ['id' => $post->user]) }}"><img src="{{ $comment->user->avatar() }}" class="comment-avatar img-circle"></a>
                                    <a href="{{ route('user', ['id' => $post->user]) }}">{{ "@" . $comment->user->profile->handle }}</a> {{ linkHashes($comment->comment) }} <br> {{ convertTimeStamp($comment->created_at) }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /blog layout #4 with image -->