<form method="POST" action="{{ route('publish') }}" enctype="multipart/form-data" id="new-post">   
    @csrf
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div id="bannerPreview" class="preview"></div>
                <br>
                <div class="text-left">
                    <label class="btn bg-danger text-white" id="reset"><i class="fa fa-times"></i> Remove Image</label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <textarea class="form-control mention" name="content" placeholder="Share what's on your mind..." id="post-text"></textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <input type="file" class="file-styled" name="media" id="banner">
            </div>
        </div>
    </div>
    <div class="form-group text-right">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn bg-indigo btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Post</button>
            </div>
        </div>
    </div>
</form>