@extends('layouts.application')

@section('css')
    <style>
        
    </style>
@endsection

@section('content')
    <form method="POST" action="{{ route('update-podium') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="blog_id" value="{{ $blog->id }}">
        <div class="form-group">
            <label class="form-label">Title</label>
            <input type="text" class="form-control" name="title" placeholder="Title your post..." required="" value="{{ $blog->title }}">
        </div>
        <div class="form-group">
            <label class="form-label">Featured Image</label>
            @if(is_object($blog->media))
                <img src="/uploads/media/{{ $blog->media->path }}" alt="" class="img-responsive" style="width:300px;">
                <br>
            @endif
            <input type="checkbox" name="noFeatured" value="1"> Check here to remove featured image<br>
            <input type="file" class="form-control" name="media" placeholder="">
        </div>
        <div class="form-group">
            <label class="form-label">Post Content</label>
            <textarea cols="18" rows="18" name="content" class="wysihtml5 wysihtml5-default form-control" placeholder="Enter content ..." required="">{{ $blog->content }}</textarea>
        </div>
        <div class="form-group" id="urlgroup">
            <label class="form-label">Post URL: https://doyou.com/pod/<span id="urlPreview">{{ $blog->url }}</label>
            <input type="text" class="form-control" name="url" placeholder="url-of-your-podium" required="" id="url" minlength="10" value="{{ $blog->url }}">
        </div>
        <div class="form-group">
            <label class="form-label">Privacy Settings</label>
            <select class="form-control" name="privacy">
                <option value="1" @if($blog->privacy == 1) selected @endif>Anyone with the link can view</option>
                <option value="2" @if($blog->privacy == 2) selected @endif>Only my followers and connections can view</option>
            </select>
        </div>
        <div class="form-group">
            <br>
            <label class="form-label">Select Subject</label>
            <select name="subject" class="form-control bg-primary-400" required="">
                <option value="">Please pick one</option>
                @foreach($subjects as $subject)
                    <option value="{{ $subject->id }}" @if($blog->subject_id == $subject->id) selected @endif>{{ $subject->title }}</option>
                @endforeach
            </select>

            <br>
            <label class="form-label">Select Shade</label>
            <select name="shade" class="form-control bg-indigo-400" required="">
                <option value="">Please pick one</option>
                @foreach($shades as $shade)
                    <option value="{{ $shade->id }}" @if($blog->shade_id == $shade->id) selected @endif>{{ $shade->title }}</option>
                @endforeach
            </select>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary" id="submit">Save Changes</button>
        </div>
    </form>
@endsection

@section('js')
    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/parsers.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="/assets/js/pages/editor_wysihtml5.js"></script>

    <script type="text/javascript">
        
    $("#url").keyup(function(){
        var val = $(this).val();
        var last = val.substr(val.length - 1);
        if(last == " "){
                $(this).val(val.slice(0,-1) + "-");
        }

        $("#urlPreview").text($(this).val());

        confirmPodURL($(this).val());
    });

    function confirmPodURL(url){
        $.post( "{{ route('confirmPodURL') }}", { url: url, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){
                if(data == "ok"){
                    $('#urlgroup').addClass("has-success");
                    $('#urlgroup').removeClass("has-error");
                    $('#submit').removeClass("disabled");
                } else{
                    alert("This URL is taken!");
                    $('#urlgroup').addClass("has-error");
                    $('#urlgroup').removeClass("has-success");
                    $('#submit').addClass("disabled");
                }
        });
    }



    </script>
@endsection
