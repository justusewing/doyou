@extends('layouts.application')

@section('css')
    <style>
        
    </style>
@endsection

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-body">
                <div class="content-group-lg">

                    @if(isset($blog->media))
                    <div class="content-group text-center">
                        <a href="#" class="display-inline-block">
                            <img src="{{ $blog->media->path() }}" class="img-responsive" alt="">
                        </a>
                    </div>
                    @endif
                    <h3 class="text-semibold mb-5">
                        <a href="#" class="text-default">{{ $blog->title }}</a>
                    </h3>
                    <ul class="list-inline list-inline-separate text-muted content-group">
                        <li>By <a href="#" class="text-muted">{{ "@" . $blog->user->profile->handle }}</a></li>
                        <li>{{ convertTimeStamp($blog->created_at) }}</li>
                        <li><a href="#" class="text-muted">12 comments</a></li>
                        <li><a href="#" class="text-muted"><i class="icon-heart6 text-size-base text-pink position-left"></i> 281</a></li>
                        <li><i class="fa fa-link"></i> <a href="{{ env('APP_URL') . "/pod/" . $blog->url }}">{{ env('APP_URL') . "/pod/" . $blog->url }}</a></li>
                    </ul>

                    {!! $blog->content !!}
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">About the author<a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            </div>

            <div class="media panel-body no-margin">
                <div class="media-left">
                    <a href="#">
                        <img src="{{ $blog->user->avatar() }}" style="width: 68px; height: 68px;" class="img-circle" alt="">
                    </a>
                </div>

                <div class="media-body">
                    <h6 class="media-heading text-semibold">{{ "@" . $blog->user->profile->handle }}</h6>
                    <p>{{ $blog->user->profile->bio }}</p>

                    <ul class="list-inline list-inline-separate no-margin">
                        <li><a href="#">All posts by James</a></li>
                        <li><a href="#">{{ $blog->user->profile->website }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="col-md-6 text-left">
                    <h1><a href="{{ route('user', ['user' => $blog->user_id]) }}"><img src="{{ $blog->user->avatar() }}" class="comment-avatar img-circle" width="50"> {{ "@" . $blog->user->profile->handle }}</a></h1>
                </div>
                <div class="col-md-6 text-right">
                    <h1>{{ convertTimeStamp($blog->created_at) }}</h1>
                </div>

                <div class="col-md-12">
                    <h1>{!! $blog->title !!}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                
            </div>
        </div>
    </div>
    -->
@endsection

@section('js')
    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/parsers.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="/assets/js/pages/editor_wysihtml5.js"></script>

    <script type="text/javascript">

    </script>
@endsection
