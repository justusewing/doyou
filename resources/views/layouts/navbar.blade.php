<div class="navbar navbar-fixed-top desktop tablet">

    <div class="navbar-header">
        <a class="navbar-brand" href="index.html"><b></b></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
    @if(!Auth::guest())
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
        @endif
        <ul class="nav navbar-nav nav-brand-head">
            <a class="navbar-brand" href="/"><b>doYou</b></a>
        </ul>
        @if(!Auth::guest())
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ Auth::user()->avatar() }}" alt="">
                        <span>{{ Auth::user()->profile->handle }}</span>
                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ route('user', ['id' => Auth::user()->id]) }}"><i class="icon-user"></i> Profile</a></li>
                        <li><a href="{{ route('user-messages') }}"><i class="fa fa-envelope"></i> Inbox</a></li>
                        <li><a href="{{ route('logout') }}"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        @endif
    </div>
</div>
@if(!Auth::guest())
<div class="navbar navbar-fixed-top mobile">
    <div class="navbar-header" style="background: white;">
        <a class="navbar-brand text-center" href="/" style="font-size:20px; line-height: 1.6">doYou</a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a href="{{ route('user', ['user' => Auth::user()->id]) }}"><img class="img-circle" src="{{ Auth::user()->avatar() }}" alt="" style="width: 30px; "></a></li>
        </ul>
    </div>
</div>
<div class="navbar navbar-fixed-bottom mobile">
    <div class="navbar-header" style="background: white;">

        <ul class="nav navbar-nav visible-xs-block">
            <li><a href="/"><i class="fa fa-compass"></i></a></li>
            <li><a href="{{ route('connections', ['user' => Auth::user()->id]) }}"><i class="fa fa fa-hands-helping"></i></a></li>
            <li><a href="#newPost" class="bg-primary" data-toggle="modal" data-target="#modal_publish"><i class="fa fa-edit text-white"></i></a></li>
            <li><a href="{{ route('user-mentions', ['user' => Auth::user()->id]) }}"><i class="fa fa-at"></i></a></li>
            <li><a href="{{ route('user-messages') }}"><i class="fa fa-inbox"></i></a></li>
        </ul>
    </div>
</div>
@endif