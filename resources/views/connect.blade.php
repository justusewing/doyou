@extends('layouts.application')

@section('css')
    <style>

        .nav-tabs-highlight li a, .nav-tabs-highlight li a i{
            
        }
        .nav-tabs.nav-justified > li > a, .nav-tabs.nav-justified > li {
            border-bottom: 0;
            border-right: 0;
        }
        .tab-content-bordered .tab-content:not([class*=bg-]){
            background: none;
            border:0;
        }
        .tab-content, .tab-pane, .tab-content-bordered .tab-content > .has-padding{
            padding:0;
        }
        .search-tab{
            width: 650px !important;
            max-width: inherit !important;
            vertical-align: middle;
            background: white;
            padding:10px
            border-bottom:1px solid silver;
            height:51px;
        }
        .nav-tabs.nav-justified > li.search-tab{
            float:right;
            border-bottom: 1px solid #ddd;
            border-left: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            border-top: 1px solid #ddd;
        }
        .search-tab .form-control{
            border: none;
            height:51px;
        }
        .search-tab .input-group-addon{
                padding: 16px 17px;
                font-size: 18px;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
            color: #2196F3;
        }
    </style>
@endsection

@section('content')
<div class="content">
<!-- Tabs widget -->
    <div class="tab-content-bordered content-group">
        <ul class="nav nav-tabs nav-tabs-highlight nav-lg nav-justified">
            <li class="active"><a href="#tab-flash" data-toggle="tab"><i class="fa fa-compass"></i> Explore</a></li>
            <li><a href="#tab-comments" data-toggle="tab"><i class="fa fa-at"></i> Mentions</a></li>
            <li><a href="#tab-questions" data-toggle="tab"><i class="fa fa-hashtag"></i> Topics</a></li>
            <li><a href="#tab-questions" data-toggle="tab"><i class="fa fa-question"></i> Questions</a></li>
            <li class="search-tab">
                <table style="width:100%; height:100%;">
                    <tr>
                        <td>
                            <div class="input-group">
                                <input type="text" class="form-control" style="font-size:18px; background:transparent;" placeholder="Search...">
                                <span class="input-group-addon bg-primary"><i class="fa fa-search"></i></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane has-padding active" id="tab-flash">
                @for($i = 0; $i < 4; $i++)
                <!-- Blog layout #4 with image -->
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="col-md-8">
                                        <div class="thumb content-group">
                                            <img src="assets/images/demo/images/blog4.jpg" alt="" class="img-responsive">
                                            <div class="caption-overflow">
                                                <span>
                                                    <a href="blog_single.html" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                        <ul class="list-inline heading-text">
                                            <li><a href="#" class="text-default"><i class="icon-comment position-left"></i>12</a></li>
                                            <li><a href="#" class="text-default"><i class="icon-twitter text-info position-left"></i> 1,489</a></li>
                                            <li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> 281</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="media">
                                            <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                            <div class="media-body">
                                                <span class="media-heading text-semibold">Victoria Baker</span>
                                                <div class="text-size-mini text-muted">
                                                    <ul class="list-inline list-inline-separate text-muted content-group">
                                                <li>By <a href="#" class="text-muted">Eugene</a></li>
                                                <li>July 5th, 2016</li>
                                                <li><a href="#" class="text-muted">image</a>, <a href="#" class="text-muted">blog</a>, <a href="#" class="text-muted">post</a></li>
                                            </ul>
                                                </div>
                                            </div>
                                            <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra despite. (read more)</p>

                                            <div class="panel-body">
                                    <ul class="media-list chat-list content-group">
                                        <li class="media reversed">
                                            <div class="media-body">
                                                <div class="media-content">Satisfactorily strenuously while sleazily</div>
                                                <span class="media-annotation display-block mt-10">2 hours ago</span>
                                            </div>

                                            <div class="media-right">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>
                                        </li>

                                        <li class="media">
                                            <div class="media-left">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face11.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <div class="media-content">Grunted smirked and grew.</div>
                                                <span class="media-annotation display-block mt-10">13 minutes ago</span>
                                            </div>
                                        </li>

                                        <li class="media reversed">
                                            <div class="media-body">
                                                <div class="media-content"><i class="icon-menu display-block"></i></div>
                                            </div>

                                            <div class="media-right">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>
                                        </li>
                                    </ul>

                                    <input name="enter-message" class="form-control content-group" placeholder="Write a comment">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <ul class="icons-list icons-list-extended mt-10">
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send photo"><i class="icon-file-picture"></i></a></li>
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send video"><i class="icon-file-video"></i></a></li>
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send file"><i class="icon-file-plus"></i></a></li>
                                            </ul>
                                        </div>

                                        <div class="col-xs-6 text-right">
                                            <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Send</button>
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                <!-- /blog layout #4 with image -->
                @endfor
            </div>

            <div class="tab-pane has-padding" id="tab-comments">
                <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="col-md-8">
                                        <div class="thumb content-group">
                                            <img src="assets/images/demo/images/blog4.jpg" alt="" class="img-responsive">
                                            <div class="caption-overflow">
                                                <span>
                                                    <a href="blog_single.html" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                        <ul class="list-inline heading-text">
                                            <li><a href="#" class="text-default"><i class="icon-comment position-left"></i>12</a></li>
                                            <li><a href="#" class="text-default"><i class="icon-twitter text-info position-left"></i> 1,489</a></li>
                                            <li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> 281</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="media">
                                            <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                            <div class="media-body">
                                                <span class="media-heading text-semibold">Victoria Baker</span>
                                                <div class="text-size-mini text-muted">
                                                    <ul class="list-inline list-inline-separate text-muted content-group">
                                                <li>By <a href="#" class="text-muted">Eugene</a></li>
                                                <li>July 5th, 2016</li>
                                                <li><a href="#" class="text-muted">image</a>, <a href="#" class="text-muted">blog</a>, <a href="#" class="text-muted">post</a></li>
                                            </ul>
                                                </div>
                                            </div>
                                            <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra despite. (read more)</p>

                                            <div class="panel-body">
                                    <ul class="media-list chat-list content-group">
                                        <li class="media reversed">
                                            <div class="media-body">
                                                <div class="media-content">Satisfactorily strenuously while sleazily</div>
                                                <span class="media-annotation display-block mt-10">2 hours ago</span>
                                            </div>

                                            <div class="media-right">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>
                                        </li>

                                        <li class="media">
                                            <div class="media-left">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face11.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <div class="media-content">Grunted smirked and grew.</div>
                                                <span class="media-annotation display-block mt-10">13 minutes ago</span>
                                            </div>
                                        </li>

                                        <li class="media reversed">
                                            <div class="media-body">
                                                <div class="media-content"><i class="icon-menu display-block"></i></div>
                                            </div>

                                            <div class="media-right">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>
                                        </li>
                                    </ul>

                                    <input name="enter-message" class="form-control content-group" placeholder="Write a comment">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <ul class="icons-list icons-list-extended mt-10">
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send photo"><i class="icon-file-picture"></i></a></li>
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send video"><i class="icon-file-video"></i></a></li>
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send file"><i class="icon-file-plus"></i></a></li>
                                            </ul>
                                        </div>

                                        <div class="col-xs-6 text-right">
                                            <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Send</button>
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>

            <div class="tab-pane has-padding" id="tab-questions">
                <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="col-md-8">
                                        <div class="thumb content-group">
                                            <img src="assets/images/demo/images/blog4.jpg" alt="" class="img-responsive">
                                            <div class="caption-overflow">
                                                <span>
                                                    <a href="blog_single.html" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                        <ul class="list-inline heading-text">
                                            <li><a href="#" class="text-default"><i class="icon-comment position-left"></i>12</a></li>
                                            <li><a href="#" class="text-default"><i class="icon-twitter text-info position-left"></i> 1,489</a></li>
                                            <li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> 281</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="media">
                                            <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                            <div class="media-body">
                                                <span class="media-heading text-semibold">Victoria Baker</span>
                                                <div class="text-size-mini text-muted">
                                                    <ul class="list-inline list-inline-separate text-muted content-group">
                                                <li>By <a href="#" class="text-muted">Eugene</a></li>
                                                <li>July 5th, 2016</li>
                                                <li><a href="#" class="text-muted">image</a>, <a href="#" class="text-muted">blog</a>, <a href="#" class="text-muted">post</a></li>
                                            </ul>
                                                </div>
                                            </div>
                                            <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra despite. (read more)</p>

                                            <div class="panel-body">
                                    <ul class="media-list chat-list content-group">
                                        <li class="media reversed">
                                            <div class="media-body">
                                                <div class="media-content">Satisfactorily strenuously while sleazily</div>
                                                <span class="media-annotation display-block mt-10">2 hours ago</span>
                                            </div>

                                            <div class="media-right">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>
                                        </li>

                                        <li class="media">
                                            <div class="media-left">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face11.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <div class="media-content">Grunted smirked and grew.</div>
                                                <span class="media-annotation display-block mt-10">13 minutes ago</span>
                                            </div>
                                        </li>

                                        <li class="media reversed">
                                            <div class="media-body">
                                                <div class="media-content"><i class="icon-menu display-block"></i></div>
                                            </div>

                                            <div class="media-right">
                                                <a href="assets/images/demo/images/3.png">
                                                    <img src="assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                </a>
                                            </div>
                                        </li>
                                    </ul>

                                    <input name="enter-message" class="form-control content-group" placeholder="Write a comment">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <ul class="icons-list icons-list-extended mt-10">
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send photo"><i class="icon-file-picture"></i></a></li>
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send video"><i class="icon-file-video"></i></a></li>
                                                <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send file"><i class="icon-file-plus"></i></a></li>
                                            </ul>
                                        </div>

                                        <div class="col-xs-6 text-right">
                                            <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Send</button>
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>
        </div>
    </div>
 <!-- /tabs widget -->
</div>
@endsection
