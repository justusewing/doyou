<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \App\Post as Posts;
use \App\Like as Likes;
use \App\Profile as Profiles;
use \App\Avatar as Avatars;
use \App\Banner as Banners;
use \App\Follow as Follows;
use \App\Connection as Connections;
use \App\Notification as Notifications;
use Auth;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function admin(){
        if($this->role == 1){
            return true;
        }
        return false;
    }

    public function profileFinished(){
        if(Profiles::where('user_id', $this->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function avatar(){
        return "/uploads/avatars/" . Avatars::where('id', Profiles::where('user_id', $this->id)->first()->avatar_id)->first()->path;
    }
    public function banner(){
        return "/uploads/banners/" . Banners::where('id', Profiles::where('user_id', $this->id)->first()->banner_id)->first()->path;
    }

    public function profile(){
        return $this->hasOne('\App\Profile');
    }

    public function blogs(){
        return $this->hasMany('\App\Blog')->orderBy('created_at', 'desc');
    }

    public function followings(){
        return $this->hasMany('\App\Follow');
    }

    public function following(){
        return Follows::where('user_id', $this->id)->get();
    }
    
    public function followers(){
        return Follows::where('following', $this->id)->get();
    }

    public function notifications(){
        return $this->hasMany('\App\Notification');
    }

    public function associates(){
        // people who sent me connection requests
        return $this->hasMany(Connections::class, 'connected_to');
    }
    public function connections(){
        // people I've sent connection requests
        return $this->hasMany(Connections::class, 'user_id');
    }

    public function sentmessages(){
        return $this->hasMany('\App\Message');
    }

    public function receivedmessages(){
        return $this->hasMany('\App\Message', 'receiver');
    }

    public function posts($query){
        return $query->has('\App\Post');
    }

    public function exploreFeed(){

    }

    public function likesPost($post){
        if(Likes::where('user_id', $this->id)->where('post_id', $post)->count() > 0){
            return true;
        }
        return false;
    }

    public function imFollowingThem($user){
        if(Follows::where('user_id', $this->id)->where('following', $user)->count() == 1){
            return true;
        }
        return false;
    }

    public function imConnectedToThem($user){
        if(Connections::where('user_id', $this->id)->where('connected_to', $user)->orWhere('connected_to', Auth::user()->id)->where('type', 1)->count() == 1){
            return true;
        }
        elseif(Connections::where('user_id', $this->id)->where('connected_to', $user)->orWhere('connected_to', Auth::user()->id)->where('type', 0)->count() == 1){
            return "pending";
        }
        return false;
    }
}
