<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog as Blogs;
use App\Subject as Subjects;
use App\Shade as Shades;
use Auth;
use DB;
use Session;
use Redirect;

class PodiumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $blogs = Auth::user()->blogs;

        foreach(Auth::user()->followings as $following){
            $blogs = $blogs->merge($following->blogs);
        }


        $data = [
            "page" => "shine",
            "podium" => "active",
            "subjects" => Subjects::all(),
            "shades" => Shades::all(),
            "blogs" => $blogs
        ];

        return view('layouts.podium.index', $data);
    }

    public function view($id){
        $data = [
            "page" => "shine",
            "podium" => "active",
            "blog" => Blogs::find($id)
        ];

        return view('layouts.podium.view', $data);
    }

    public function edit($id){
        $blog = Blogs::find($id);

        if($blog->user_id == Auth::user()->id){
            $data = [
                "page" => "shine",
                "podium" => "active",
                "subjects" => Subjects::all(),
                "shades" => Shades::all(),
                "blog" => $blog
            ];

            return view('layouts.podium.edit', $data);
        } else {
            return Redirect::back();
        }
    }

    public function new(){
    	$data = [
    		"page" => "shine",
            "podium" => "active",
            "subjects" => Subjects::all(),
            "shades" => Shades::all()
    	];

    	return view('layouts.podium.new', $data);
    }

    public function publish(Request $request){
    	$data = [
    		"page" => "shine",
            "podium" => "active",
    	];

        $mediaDestination = public_path() . "/uploads/media";

        if($request->file('media') !== null){

            $media = $request->file('media');
            $mediaName = generateRandomString() . "-" . $media->getClientOriginalName();
            $media->move($mediaDestination, $mediaName); 

            DB::table('media')->insert([
                "user_id" => Auth::user()->id,
                "path" => $mediaName
            ]);

            $media_id = DB::getPdo()->lastInsertId();

            Blogs::create([
                "user_id" => Auth::user()->id,
                "title" => $request->title,
                "content" => $request->content,
                "media_id" => $media_id,
                "subject_id" => $request->subject,
                "shade_id" => $request->shade,
                "url" => $this->urlize($request->url),
                "privacy" => $request->privacy
            ]);

        } else {

            Blogs::create([
                "user_id" => Auth::user()->id,
                "title" => $request->title,
                "content" => $request->content,
                "subject_id" => $request->subject,
                "shade_id" => $request->shade,
                "url" => $this->urlize($request->url),
                "privacy" => $request->privacy
            ]);
        }

    	return Redirect::to('/podium');
    }

    public function update(Request $request){

        $mediaDestination = public_path() . "/uploads/media";

        DB::table('blogs')->where('id', $request->blog_id)->where('user_id', Auth::user()->id)->update([
            "title" => $request->title,
            "content" => $request->content,
            "subject_id" => $request->subject,
            "shade_id" => $request->shade,
            "url" => $this->urlize($request->url),
            "privacy" => $request->privacy
        ]);

        if($request->noFeatured == 1){
            DB::table('blogs')->where('id', $request->blog_id)->where('user_id', Auth::user()->id)->update([
                "media_id" => NULL
            ]);
        }

        if($request->file('media') !== null){

            $media = $request->file('media');
            $mediaName = generateRandomString() . "-" . $media->getClientOriginalName();
            $media->move($mediaDestination, $mediaName); 

            DB::table('media')->insert([
                "user_id" => Auth::user()->id,
                "path" => $mediaName
            ]);

            $media_id = DB::getPdo()->lastInsertId();

            DB::table('blogs')->where('id', $request->blog_id)->where('user_id', Auth::user()->id)->update([
                "media_id" => $media_id
            ]);

        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Nice!",
            "body" => "Your pod was updated"
        ]);

        Return Redirect::back();
    }

    public function confirmPodURL(Request $request){
        if(Blogs::where('url', $this->urlize($request->url) )->count() == 0){
            return "ok";
        }
        return "error";
    }

    public function urlize($url){
        return str_replace(" ", "-", strtolower($url));
    }
}
