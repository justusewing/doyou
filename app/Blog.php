<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'user_id', 'title', 'media_id', 'content', 'type', 'status', 'subject_id', 'shade_id', 'url', 'privacy'
    ];

    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function media(){
    	return $this->hasOne('\App\Media', 'id', 'media_id');
    }

    public function subject(){
    	return $this->hasOne('\App\Subject', 'id', 'subject_id');
    }

    public function shade(){
    	return $this->hasOne('\App\Shade', 'id', 'shade_id');
    }

    public function scopeSearch($q){
         return empty(request()->search) ? $q : $q->where('title', 'like', '%'.request()->search.'%')->orWhere('content', 'like', '%'.request()->search.'%');

    }
}
