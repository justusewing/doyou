<?php

namespace App\Services\Email;

use Illuminate\Support\Facades\Facade;

class EmailFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Email';
    }
}