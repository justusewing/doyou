<?php

namespace App\Services\Notify;
use Auth;
use DB;
use App\Message as Messages;
use App\User as Users;
use App\Notification as Notifications;

class Notify
{
    public function followedYou($sender, $receiver){
        Notifications::create([
            "user_id" => $receiver,
            "from_id" => $sender,
            "type" => 4
        ]);
    }

    public function wantsToConnect($sender, $receiver){
        Notifications::create([
            "user_id" => $receiver,
            "from_id" => $sender,
            "type" => 5
        ]);
    }

    public function nowConnected($sender, $receiver){

    }

	public function postCommentNewThread($sender, $receiver, $post){
        Notifications::create([
            "user_id" => $receiver,
            "from_id" => $sender,
            "post_id" => $post,
            "type" => 7
        ]);
    }

    public function likedPost($sender, $receiver, $post){
        Notifications::create([
            "user_id" => $receiver,
            "from_id" => $sender,
            "post_id" => $post,
            "type" => 3
        ]);
    }

    public function mentionedYouPublish($sender, $receiver, $post){
        Notifications::create([
            "user_id" => $receiver,
            "from_id" => $sender,
            "post_id" => $post,
            "type" => 6
        ]);
    }


}