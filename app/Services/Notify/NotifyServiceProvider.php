<?php

namespace App\Services\Notify;

use Illuminate\Support\ServiceProvider;

class NotifyServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Notify', function($app) {
            return new Notify();
        });
    }
}