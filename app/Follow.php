<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\User as Users;

class Follow extends Model
{
	protected $fillable = [
        'user_id', 'following', 'type',
    ];

    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function posts(){
    	return $this->hasMany('\App\Post', 'user_id', 'following');
    }

    public function blogs(){
        return $this->hasMany('\App\Blog', 'user_id', 'following')->orderBy('created_at', 'desc');
    }

    public function identity($user){
    	return Users::where('id', $user)->first();
    }
}