<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = [
        'user_id', 'post_id', 'comment', 'type', 'status', 'comment_id',
    ];

    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function post(){
    	return $this->belongsTo('\App\Post');
    }

    public function scopeReplies($query){
    	return $query->where('comment_id', $this->id)->get();
    }
}