<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Media as Media;
use Auth;

class Post extends Model
{

	public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function featured(){
    	return Media::where('id', $this->featured)->first()->path;
    }

    public function comments(){
        return $this->hasMany('\App\Comment')->where('comment_id', NULL)->orderBy('created_at', 'desc');
    }

    public function likes(){
        return $this->hasMany('\App\Like');
    }

    public function isOwner(){
        if($this->user_id == Auth::user()->id){
            return true;
        }

        return false;
    }

    public function scopeSearch($q){
         return empty(request()->search) ? $q : $q->where('title', 'like', '%'.request()->search.'%')->orWhere('content', 'like', '%'.request()->search.'%');

    }
}
